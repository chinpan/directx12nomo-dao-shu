/*=========================================================

	[Application.h]
	Atuhor : 出合翔太

=========================================================*/

#pragma once
#include<Windows.h>
#include<tchar.h>
#include<d3d12.h>
#include<dxgi1_6.h>
#include<DirectXMath.h>
#include<vector>
#include<map>
#include<d3dcompiler.h>
#include<DirectXTex.h>
#include<d3dx12.h>
#include<wrl.h>
#include<memory>

// シングルトンクラス
class Application
{
private:
	// ウィンドウ関連
	WNDCLASSEX _windowClass;
	HWND _hwnd;

	// ゲーム用ウィンドウの生成
	void CreateGameWindow(HWND& hwnd, WNDCLASSEX& windowSlass);
	// コンストラクタ
	Application(); // シングルトンだからprivateにする

	// コピーと代入を禁止する
	Application(const Application&) = delete;
	void operator= (const Application&) = delete;

public:
	// インスタンスの取得
	static Application& Instance(); 
	// 初期化
	bool Init();			
	// アプリケーションループ
	void ApiRun();				
	// 終了処理
	void Uninit();				
	// 画面サイズの取得
	SIZE GetWindowSize()const;		
};

