/*=================================================

	[Dx12Wrapper.cpp]
	Author : 出合翔太

==================================================*/

#include"Dx12Wrapper.h"
#include<cassert>
#include<d3dx12.h>
#include"Application.h"

#pragma comment(lib,"DirectXTex.lib")
#pragma comment(lib,"d3d12.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"d3ddcompiler.lib")

using namespace Microsoft::WRL;
using namespace std;
using namespace DirectX;

// 無名名前空間
namespace
{
	/// <summary>
	/// モデルのパスとテクスチャファイルのパスから合成パスを得る
	/// </summary>
	/// <paramname = "modelPath"> アプリケーションからみたpmdモデルのパス </paramname>
	/// <paramname = "texPath"> PMDモデルから見たテクスチャのパス </paramname>
	/// <returns> アプリケーションから見たテクスチャパス </returns>
	std::string GetTexturePathfromModelAndTexPath(const std::string& modelPath, const char* texPath)
	{
		int pathIndex1 = modelPath.rfind("/");
		int pathIndex2 = modelPath.rfind("\\");
		auto pathIndex = max(pathIndex1, pathIndex2);
		auto folderPath = modelPath.substr(0, pathIndex + 1);
		return folderPath + texPath;
	}

	/// <summary>
	/// ファイル名から拡張子を取得する
	/// </summary>
	/// <paramname = "path"> 対象パス文字列 </paramname>
	/// <returns> 拡張子 </returns>
	string GetExtension(const std::string& path)
	{
		int idx = path.rfind('.');
		return path.substr(idx + 1, path.length() - idx - 1);
	}

	/// <summary>
	/// ファイル名から拡張子を取得する:ワイド文字列
	/// </summary>
	/// <paramname = "path"> 対象パス文字列 </paramname>
	/// <returns> 拡張子 </returns>
	wstring GetExtension(const std::wstring& path)
	{
		int idx = path.rfind(L'.');
		return path.substr(idx + 1, path.length() - idx - 1);
	}

	/// <summary>
	/// テクスチャのパスをセパレータ文字で分離する
	/// </summary>
	/// <paramname = "path"> 対象パス文字列 </paramname>
	/// <paramname = "splitter"> 区切り文字 </paramname>
	/// <returns> 分離前後の文字列ペア <returns>
	pair <string, string> SplitFileName(const std::string& path, const char splitter = '*')
	{
		int idx = path.find(splitter);
		pair<string, string>ret;
		ret.first = path.substr(0, idx);
		ret.second = path.substr(idx + 1, path.length() - idx - 1);
		return ret;
	}

	/// <sumamry>
	/// string(マルチバイト文字列)からwstring(ワイド文字列)を得る
	/// </summary>
	/// <paramname = "str"> マルチバイト文字列 </paramname>
	/// <returns> 変換されたワイド文字列 </returns>
	std::wstring GetwideStringFromString(const std::string& str)
	{
		// 呼び出し１回目（文字列数を得る）
		auto num1 = MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED | MB_ERR_INVALID_CHARS, str.c_str(), -1, nullptr, 0);
		std::wstring wstr; // stringのwchar_t版
		wstr.resize(num1); // 得られた文字列数でリサイズ
		// 呼び出し2回目（確保済みのwstrに変換文字列をコピーする）
		auto num2 = MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED | MB_ERR_INVALID_CHARS,str.c_str(), -1, &wstr[0], num1);
		assert(num1 == num2); // チェック
		return wstr;
	}

	/// <summary>
	/// デバッグレイヤーの有効化
	/// </summary>
	void EnableDebugLayer()
	{
		ComPtr<ID3D12Debug> debugLayer = nullptr;
		auto result = D3D12GetDebugInterface(IID_PPV_ARGS(&debugLayer));
		debugLayer->EnableDebugLayer();
	}
}

// レンダーターゲットの生成
HRESULT Dx12Wrapper::Create_RenderTargets()
{
	// スワップチェーンの生成
	DXGI_SWAP_CHAIN_DESC1 desc = {};
	auto result = _swapchain->GetDesc1(&desc);
	
	// ディスクリプタヒープの作成
	D3D12_DESCRIPTOR_HEAP_DESC heapdesc = {};
	heapdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV; // レンダーターゲットビュー -> RTVを指定
	heapdesc.NodeMask = 0;
	heapdesc.NumDescriptors = 2; // 表裏の２つ
	heapdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE; // 特になし
	
	result = _dev->CreateDescriptorHeap(&heapdesc, IID_PPV_ARGS(_rtvHeaps.ReleaseAndGetAddressOf()));
	if (FAILED(result))
	{
		SUCCEEDED(result);
		return result;
	}

	// スワップチェーンのメモリとひも付け
	DXGI_SWAP_CHAIN_DESC swcDesc = {};
	result = _swapchain->GetDesc(&swcDesc);
	_backBuffers.resize(swcDesc.BufferCount);
	D3D12_CPU_DESCRIPTOR_HANDLE handle = _rtvHeaps->GetCPUDescriptorHandleForHeapStart();

	//SRGBレンダーターゲットビュー設定
	D3D12_RENDER_TARGET_VIEW_DESC rtvDesc = {};
	rtvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
	rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
	for (int nCntIdx = 0; nCntIdx < swcDesc.BufferCount; nCntIdx++)
	{
		result = _swapchain->GetBuffer(nCntIdx, IID_PPV_ARGS(&_backBuffers[nCntIdx]));
		assert(SUCCEEDED(result));
		rtvDesc.Format = _backBuffers[nCntIdx]->GetDesc().Format;
		// レンダーターゲットビューの生成
		_dev->CreateRenderTargetView(_backBuffers[nCntIdx], &rtvDesc, handle);
		handle.ptr += _dev->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	}
	_viewport.resst(new CD3DX12_VIEWPORT(_backBuffers[0]));
}

// デプスステンシルビューの生成
HRESULT Dx12Wrapper::Create_DepthStencilView()
{
	return E_NOTIMPL;
}

// スワップチェインの生成
HRESULT Dx12Wrapper::Create_SwapChain(const HWND& hwnd)
{
	return E_NOTIMPL;
}

// DXGI関連の初期化
HRESULT Dx12Wrapper::Init_DXGIDevice()
{
	return E_NOTIMPL;
}

// コマンド関連の初期化
HRESULT Dx12Wrapper::Init_Command()
{
	return E_NOTIMPL;
}

// ビュープロジェクション用ビューの生成
HRESULT Dx12Wrapper::Create_SceneView()
{
	return E_NOTIMPL;
}

//テクスチャローダテーブルの作成
void Dx12Wrapper::Create_TextureLoaderTable()
{
}

//テクスチャ名からテクスチャバッファ作成、中身をコピー
ID3D12Resource* Dx12Wrapper::Create_TextureFromFile(const char* texpath)
{
	return nullptr;
}

Dx12Wrapper::Dx12Wrapper(HWND hwnd)
{
}

void Dx12Wrapper::BeginDraw()
{
}

void Dx12Wrapper::EndDraw()
{
}

ComPtr<ID3D12Resource> Dx12Wrapper::GetTextureByPath(const char* texpath)
{
	return ComPtr<ID3D12Resource>();
}

ComPtr<ID3D12Device> Dx12Wrapper::Device()
{
	return ComPtr<ID3D12Device>();
}

ComPtr<ID3D12GraphicsCommandList> Dx12Wrapper::commandList()
{
	return ComPtr<ID3D12GraphicsCommandList>();
}

ComPtr<IDXGISwapChain4> Dx12Wrapper::Swapchain()
{
	return ComPtr<IDXGISwapChain4>();
}

void Dx12Wrapper::SetScene()
{
}
