/*=================================================

	[Dx12Wrapper.h]
	Author : 出合翔太

==================================================*/

#pragma once
#include<d3d12.h>
#include<dxgi1_6.h>
#include<map>
#include<unordered_map>
#include<DirectXTex.h>
#include<wrl.h>
#include<string>
#include<functional>

class Dx12Wrapper
{
private:
	SIZE _winSize;
	template<class T>
	using ComPtr = Microsoft::WRL::ComPtr<T>;

	// DXGI関連
	ComPtr<IDXGIFactory4> _dxgiFactory = nullptr; // DXGIインターフェイス
	ComPtr<IDXGISwapChain4> _swapchain = nullptr; // スワップチェイ

	// DirectX12関連
	ComPtr<ID3D12Device> _dev = nullptr;					//デバイス
	ComPtr<ID3D12CommandAllocator> _cmdAllocator = nullptr; // コマンドアロケータ
	ComPtr<ID3D12GraphicsCommandList> _cmdList = nullptr;	// コマンドリスト
	ComPtr<ID3D12CommandQueue> _cmdQueue = nullptr;			// コマンドキュー

	// バッファ関連
	ComPtr<ID3D12Resource> _depthBuffer = nullptr;		// 深度バッファ
	std::vector<ID3D12Resource*> _backBuffers;			// バックバッファ
	ComPtr<ID3D12DescriptorHeap> _rtvHeaps = nullptr;	// レンダーターゲット用デスクリプタヒープ
	ComPtr<ID3D12DescriptorHeap> _dsvHeaps = nullptr;	// 深度バッファビュー用デスクリプタヒープ
	std::unique_ptr<D3D12_VIEWPORT> _viewport;			// ビューポート
	std::unique_ptr<D3D12_RECT> _scissorrect;			// シザー矩形

	// シーンを構成するバッファ
	ComPtr<ID3D12Resource> _sceneConstBuff = nullptr; 
	struct SceneData
	{
		DirectX::XMMATRIX view; // ビュー行列
		DirectX::XMMATRIX proj; // プロジェクション行列
		DirectX::XMMATRIX eye;	// 視点座標
	};
	SceneData* _mappedSceneData;
	ComPtr<ID3D12DescriptorHeap> _sceneDescHeap = nullptr;

	// フェンス
	ComPtr<ID3D12Fence> _fence = nullptr;
	UINT64 _fenceVal = 0;
	
	// レンダーターゲットの生成
	HRESULT Create_RenderTargets();
	// デプスステンシルビューの生成
	HRESULT Create_DepthStencilView();
	// スワップチェインの生成
	HRESULT Create_SwapChain(const HWND& hwnd);
	// DXGI関連の初期化
	HRESULT Init_DXGIDevice();
	// コマンド関連の初期化
	HRESULT Init_Command();
	// ビュープロジェクション用ビューの生成
	HRESULT Create_SceneView();

	
	//ロード用テーブル
	using LoadLambda_t = std::function<HRESULT(const std::wstring & path, DirectX::TexMetadata*, DirectX::ScratchImage&)>;
	std::map < std::string, LoadLambda_t> _loadLambdaTable;
	//テクスチャテーブル
	std::unordered_map<std::string, ComPtr<ID3D12Resource>> _textureTable;
	//テクスチャローダテーブルの作成
	void Create_TextureLoaderTable();
	//テクスチャ名からテクスチャバッファ作成、中身をコピー
	ID3D12Resource* Create_TextureFromFile(const char* texpath);
	
public:
	Dx12Wrapper(HWND hwnd);
	void BeginDraw();
	void EndDraw();

	/// テクスチャパスから必要なテクスチャバッファへのポインタを返す
	/// @ <Paramname> "texpath"  テクスチャファイルパス
	ComPtr<ID3D12Resource> GetTextureByPath(const char* texpath);
	ComPtr<ID3D12Device> Device();					 // デバイス
	ComPtr<ID3D12GraphicsCommandList> commandList(); // コマンドリスト
	ComPtr<IDXGISwapChain4> Swapchain();			 // スワップチェイン
	void SetScene();
};

