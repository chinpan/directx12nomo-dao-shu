/*=========================================================

	[Application.h]
	Atuhor : 出合翔太

=========================================================*/
#include "Application.h"

// ウィンドウ定数
const unsigned int window_width = 1920;
const unsigned int window_height = 1080;

// コールバック関数
LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	// ウィンドウ破棄
	if (msg == WM_DESTROY)
	{
		PostQuitMessage(0);	// アプリの終了をOSに伝える
		return 0;
	}
	return DefWindowProc(hwnd, msg, wparam, lparam); // 既定処理
}

// ゲーム用ウィンドウの生成
void Application::CreateGameWindow(HWND& hwnd, WNDCLASSEX& windowClass)
{
	HINSTANCE hInst = GetModuleHandle(nullptr);
	// ウィンドウクラスの生成＆登録
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.lpfnWndProc = (WNDPROC)WindowProcedure; // コールバック関数の指定
	windowClass.lpszClassName = _T("DirectX12"); // アプリケーションクラス名
	windowClass.hInstance = GetModuleHandle(0); // ハンドルの取得
	RegisterClassEx(&windowClass); // アプリケーションクラス

	RECT wrc = { 0,0,window_width,window_height }; // ウィンドウサイズ
	AdjustWindowRect(&wrc, WS_OVERLAPPEDWINDOW, false); // ウィンドウサイズの補正
	// ウィンドウオブジェクト
	hwnd = CreateWindow(windowClass.lpszClassName,	// クラス名
		_T("ゲームアプリ"),							// タイトルバー
		WS_OVERLAPPEDWINDOW,						// ウィンドウタイプ
		CW_USEDEFAULT, CW_USEDEFAULT,				// 表示XY座標はOSに任せる
		wrc.right - wrc.left, wrc.bottom - wrc.top,	// ウィンドウ幅と高さ
		nullptr, nullptr,							// 親ウィンドウハンドルとメニューハンドル
		windowClass.hInstance,						// 呼び出しアプリケーションハンドル
		nullptr);									// 追加パラメータ
}

// インスタンスの取得
Application& Application::Instance()
{
	static Application instance;
	return instance;
}

// 初期化
bool Application::Init()
{
	auto result = CoInitializeEx(0, COINITBASE_MULTITHREADED);
	CreateGameWindow(_hwnd, _windowClass);
	// DirectX12ラッパー生成＆初期化
	
}

// アプリケーションループ
void Application::ApiRun()
{
}

// 終了処理
void Application::Uninit()
{
}

// 画面サイズの取得
SIZE Application::GetWindowSize() const
{
	SIZE ret;
	ret.cx = window_width;
	ret.cy = window_height;
	return ret;
}
